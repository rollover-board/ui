# build 
FROM node:12.22.11-buster as build-stage
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
COPY src/components/config-k8s.json /app/src/components/config.json
RUN npm run build

# Nginx run stage
FROM nginx:latest
COPY --from=build-stage /app/build/ /usr/share/nginx/html 

COPY nginx.conf /etc/nginx/conf.d/default.conf
