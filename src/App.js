import React from 'react';
import PostEditor from "./components/PostEditor";
import PostStack from "./components/PostStack";

function App() {
  return (
    <div className = "post-container">
      <PostEditor />
      <PostStack/>      
    </div>
  );
}

export default App;
