import React, { useEffect, useState } from 'react';
import Post from './Post';
import axios from 'axios';
import configData from "./config.json";

// Facilitate sorting by time
function comparePostTimes(a, b) {

  if (a.created < b.created) {
    return -1;
  }
  if (a.created > b.created) {
    return 1;
  }
  return 0;
}

// format the posts taken from the web service
function importPosts(arr) {

  // convert rfc3339 to millis
  for (var i = 0; i < arr.length; i++) {
    arr[i].created = Date.parse(arr[i].created)
  }

  arr = arr.sort(comparePostTimes)

  return arr;
}

export default function PostStack(props) {
  const [posts, setPosts] = useState([]);
  var url = configData.POSTS_URL;
  if (url.charAt(url.length-1) === '/' ){
    url = url.slice(0,-1)
  }
  var fullUrl = url+'/posts'


  useEffect(() => {
    setTimeout(() => {

      console.log("Requesting posts from "+fullUrl)
      axios.get(fullUrl).then(resp => {
        
        if (resp.data === undefined) {
          console.log("received no data")
        }else{
          var arr = resp.data
          arr.posts = importPosts(arr.posts)
          setPosts(arr.posts)
        }

      })
        .catch(err => {
          console.log("err in request");
          console.log(err)
          console.error(err);
        });
    }, 2000);
  });

  return <div className="posts-stack">{posts.map(p => (
    <ul>
      <Post key={p.id} title={p.title} content={p.content} />
    </ul>
  ))} </div>;
}
