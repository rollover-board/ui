import React from "react";

export default function Post (props) {
    return (
        <div className = "post">
            <div className = "post-title">
                {props.title}
            </div>

            <div className = "post-content">
                {props.content}
            </div>
        </div>

    );
}