import React, { useState } from 'react';
import axios from 'axios';
import configData from "./config.json";

export default function PostEditor(props) {
  const [content, setContent] = useState("");
  const [title, setTitle] = useState("");

  const handleSubmit = event => {
    event.preventDefault();

    if (title.length < 1 && content.length < 1) {
      console.log("Empty post");
      return;
    }
    if (content.length > configData.MAX_POST_LEN) {
      console.log("Message content too long");
      return;
    }

    if (title.length > configData.MAX_TITLE_LEN) {
      console.log("Message title too long");
      return;
    }

    var url = configData.POSTS_URL
    axios.post(url, { "title": title, "content": content }).then(resp => {
      console.log(resp);
      setContent("");
      setTitle("");
      title = "";
    })
      .catch(err => {
        console.log("err in posting data");
        console.error(err);
      });
  }

  return (
    <div className="postEditor">
      <label>
        Post Something
      </label>
      <input name="postTitle" className="postTitle" onChange={(event) => setTitle(event.target.value)} />

      <textarea name="postContent" className="postContent" onChange={(event) => setContent(event.target.value)} />
      <button onClick={handleSubmit}>Post</button>
    </div>
  )
}